package pachetControlSimulare;

import java.util.ArrayList;

import interfataGrafica.LogText;
import interfataGrafica.Statistici;
import model.Client;
import model.Scheduler;

public class SimulationManager implements Runnable {

	private int timpSimulare;
	private int maxProcesare;
	private int minProcesare;
	private int maxDelay;
	private int minDelay;
	private int numarServere;
	private int numarClienti;

	private static int curent;

	private ArrayList<Client> clienti = new ArrayList<>();

	private Scheduler scheduler;
	private LogText logger;

	public SimulationManager(int timpSimulare, int maxProcesare, int minProcesare, int maxDelay, int minDelay,
			int nrServ, int nrClienti) {
		this.timpSimulare = timpSimulare;
		this.maxProcesare = maxProcesare;
		this.minProcesare = minProcesare;
		this.maxDelay = maxDelay;
		this.minDelay = minDelay;
		this.numarServere = nrServ;
		this.numarClienti = nrClienti;

		this.logger = new LogText(this.numarServere);
		this.genereazaClienti();
		scheduler = new Scheduler(this.numarServere, 5, this.logger);

	}

	private void genereazaClienti() {
		int arrive = 1;
		int procesare = 0;
		int difProcesare = this.maxProcesare - this.minProcesare;

		for (int i = 1; i <= this.numarClienti; i++) {
			procesare = (int) (Math.random() * difProcesare) + this.minProcesare;
			Client client = new Client(i, arrive, procesare);
			arrive += (int) (Math.random() * (this.maxDelay - this.minDelay)) + this.minDelay;
			this.clienti.add(client);
		}

		for(Client x: this.clienti) {
			this.logger.displayData(x.getArrive(), x.getProcesare(), 2, x.getId());
		}
	}

	@Override
	public void run() {
		try {
			for (curent = 0; curent <= this.timpSimulare; curent++) {

				Thread.sleep(1000);
				for (Client client : this.clienti) {
					if (client.getArrive() == curent) {
						scheduler.alegereServer(client);
						//this.clienti.remove(client);
						//break;
					}
				}

				
			}
			Thread.sleep(3500);
			@SuppressWarnings("unused")
			Statistici stat = new Statistici(scheduler, numarClienti, timpSimulare);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public synchronized static int getCurent() {
		return curent;
	}
}

package model;

public class Client {
	
	private int id;
	private int arrive;
	private int procesare;
	private int waiting;
	
	private int finishTime;
	
	public Client(int id, int arrive, int procesare) {
		this.id = id;
		this.arrive = arrive;
		this.procesare = procesare;
	}

	
	public int getId() {
		return id;
	}

	public int getArrive() {
		return arrive;
	}

	public int getProcesare() {
		return procesare;
	}

	public int getFinishTime() {
		return finishTime;
	}
	
	public int getWaiting() {
		return waiting;
	}

	public void setWaiting(int x) {
		this.waiting = x;
		this.finishTime = this.arrive + this.procesare + this.waiting;
	}

	public void setFinishTime(int finishTime) {
		this.finishTime = finishTime;
	}
	
	
}

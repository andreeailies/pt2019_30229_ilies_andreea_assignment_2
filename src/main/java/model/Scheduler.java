package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import interfataGrafica.LogText;
import pachetControlSimulare.SimulationManager;

public class Scheduler {

	private List<Server> servere = Collections.synchronizedList(new ArrayList<Server>());
	private int numarServere;
	private int capacitateServer;
	
	public int peak;
	
	private LogText logger;

	public Scheduler(int numarServere, int capacitateServer, LogText x) {
		this.numarServere = numarServere;
		this.capacitateServer = capacitateServer;
		logger = x;

		for (int i = 1; i <= this.numarServere; i++) {
			Server s = new Server(this.capacitateServer, i, logger);
			Thread t = new Thread(s);
			servere.add(s);
			t.start();
		}
		
		
	}

	public void alegereServer(Client client) {
		Collections.sort(this.servere);
		for(Server x : this.servere) {
			if (x.getClienti().remainingCapacity() > 0) {
				x.addClient(client);
				
				x.satisfiedClients++;
				x.serviceStat += client.getProcesare();
				x.waitingStat += client.getWaiting();
				
				logger.displayData(x.getId(),client.getId(),1,SimulationManager.getCurent());
				this.peak = logger.getPeak();
				break;
			}
		}
	}


	public List<Server> getServere() {
		return servere;
	}

	public int getNumarServere() {
		return numarServere;
	}

	public int getCapacitateServer() {
		return capacitateServer;
	}

}

package model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import interfataGrafica.LogText;

public class Server implements Runnable, Comparable<Server> {

	private int id;

	private BlockingQueue<Client> clienti;
	private AtomicInteger asteptare;
	
	private LogText logger;
	
	public int waitingStat;
	public int serviceStat;
	public int satisfiedClients;

	public Server(int capacitate, int id, LogText x) {
		waitingStat = serviceStat = satisfiedClients = 0;
		clienti = new ArrayBlockingQueue<Client>(capacitate, true);
		this.id = id;
		asteptare = new AtomicInteger(0);
		logger = x;
	}

	public synchronized void addClient(Client client) {
		client.setWaiting(asteptare.get());
		clienti.add(client);
		asteptare.addAndGet(client.getProcesare());
	}

	@Override
	public void run() {

		while (true) {
			try {
				Client client = clienti.take();
			
				for (int curent = 0; curent < client.getProcesare(); curent++) {
					try {
						Thread.sleep(1000);
						asteptare.decrementAndGet();
					} catch (InterruptedException e) {

						e.printStackTrace();
					}
				}
				logger.displayData(this.getId(),client.getId(),0,client.getFinishTime());	
			
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}

	public BlockingQueue<Client> getClienti() {
		return clienti;
	}

	public int getAsteptare() {
		return asteptare.get();
	}

	public int getId() {
		return this.id;
	}

	@Override
	public int compareTo(Server o) {
		int astep1 = this.getAsteptare();
		int astep2 = o.getAsteptare();
		if (astep1 > astep2)
			return 1;
		if (astep1 < astep2)
			return -1;
		if (this.id < o.id)
			return -1;
		if (this.id > o.id)
			return 1;
		return 0;
	}

}

package interfataGrafica;

import java.awt.Color;
import java.awt.Dimension;
//import java.awt.event.AdjustmentEvent;
//import java.awt.event.AdjustmentListener;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class LogText extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panou = new JPanel();
	private Vector<String> x = new Vector<>();
	private Vector<String> afisareCozi = new Vector<>();
	
	private int nrCozi;
	private int active = 0;
	private int peak = 0;
	
	public LogText(int nrCozi) {
		this.setTitle("Progres cozi");
		this.nrCozi = nrCozi;
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setSize(new Dimension(500, 500));
		
		this.setContentPane(panou);
		panou.setMinimumSize(new Dimension(500, 500));
		panou.setBackground(Color.black);
		panou.setLayout(new BoxLayout(panou, BoxLayout.Y_AXIS));
		
		for(int i = 1; i <= this.nrCozi; i++) {
			afisareCozi.add("Q" + i + ": ");
		}
	}
	
	
	public synchronized void displayData(int coada, int client, int operatie, int timp) {
		panou.removeAll(); 
		
		if (operatie == 1) {
			x.addElement("Coada " + coada + ": clientul " + client + " a intrat (" + timp + ")");
		} else 
		if (operatie == 0){
			x.addElement("Coada " + coada + ": clientul " + client + " a terminat (" + timp + ")");
		} else {
			x.addElement("Clientul " + timp + ": ajunge la " + coada + " si are nevoie de " + client + " sec");
		}
		//System.out.println("Coada " + coada + ": clientul " + client + " " + operatie + " (" + timp + ")");
		
		JList<String> lista = new JList<>(x);
		JScrollPane scroll = new JScrollPane(lista);
//		scroll.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {  
//	        public void adjustmentValueChanged(AdjustmentEvent e) {  
//	            e.getAdjustable().setValue(e.getAdjustable().getMaximum());  
//	        }
//	    });
		
		this.setContentPane(panou);
		panou.add(new JLabel(" "));
		panou.add(scroll); 
		panou.add(new JLabel(" "));
		
		lista.setMaximumSize(new Dimension(400,300)); 
		scroll.setMaximumSize(new Dimension(400, 300));
		panou.setMinimumSize(new Dimension(500, 500));
		
		scroll.setVisible(true);
		lista.setVisible(true);
		this.setVisible(true);
		
		if (operatie != 2) afisareCozi(coada, client, operatie, timp);
		
		panou.revalidate();
		
	}
	
	private synchronized void afisareCozi(int coada, int client, int operatie, int time) {
		String curent = afisareCozi.elementAt(coada-1);
		if (operatie == 1) {
			// intra
			curent = curent + "| C" + client; 
		} else {
			// iese
			curent = curent.replace("| C" + Integer.toString(client), "");
		}
		afisareCozi.set(coada-1, curent);
		
		JPanel panouCozi = new JPanel();
		panouCozi.setLayout(new BoxLayout(panouCozi, BoxLayout.Y_AXIS));
		panouCozi.setVisible(true);
		panouCozi.setAlignmentX(CENTER_ALIGNMENT);
		panouCozi.setMinimumSize(new Dimension(250, 100));
		
		int ok = 0;
		for (String codita : this.afisareCozi) {
			JTextField tField = new JTextField(codita);
			tField.setMaximumSize(new Dimension(250, 30));
			tField.setAlignmentX(CENTER_ALIGNMENT);
			panouCozi.add(tField);
			tField.setVisible(true);
			this.setVisible(true);	
			
			if (codita.length() >= 5) ok++;
		}
		
		if (ok > this.active) {
			this.active = ok;
			this.peak = time;
		}
		panou.add(panouCozi);
		panou.revalidate();
	}
	
	public int getPeak() {
		return this.peak;
	}

}

package interfataGrafica;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Scheduler;
import model.Server;

public class Statistici extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Scheduler scheduler;

	private float avgService = 0;
	private ArrayList<Float> queueService = new ArrayList<>();
	private float avgWaiting = 0;
	private ArrayList<Float> queueWaiting = new ArrayList<>();
	private int peakTime = 0;
	private int emptyTime = 0;
	

	
	private JPanel panou = new JPanel();
	
	public Statistici(Scheduler sch, int nrClienti, int timp) {
		this.scheduler = sch;
		
		for(Server sv: this.scheduler.getServere()) {
			if (sv.satisfiedClients != 0) {
			queueService.add((float)sv.serviceStat / sv.satisfiedClients);
			queueWaiting.add((float)sv.waitingStat / sv.satisfiedClients);
			} else {
				queueService.add(0.f);
				queueWaiting.add(0.f);
			}
			avgService += sv.serviceStat;
			avgWaiting += sv.waitingStat;
			emptyTime += timp - sv.serviceStat;
		}
		
		if (nrClienti != 0) {
			avgService = (float)avgService / nrClienti;
			avgWaiting = (float)avgWaiting / nrClienti;
		}
		else {
			avgService = avgWaiting = 0;
		}
		
		peakTime = sch.peak;
		
		afisare();
	}
	
	private void afisare() {
		this.setTitle("Statistici");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setSize(new Dimension(500, 300));
		this.setContentPane(panou);
		panou.setMinimumSize(new Dimension(500, 300));
		panou.setBackground(Color.yellow);
		panou.setLayout(new BoxLayout(panou, BoxLayout.Y_AXIS));
		
		String avg = "Serviciul mediu:";
		for(Float x: queueService) {
			avg += Float.toString(x) + ", ";
		}
		avg += " (total: " + Float.toString(avgService) + ")";
		JTextField serv = new JTextField(avg);
		
		avg = "Asteptarea medie:";
		for(Float x: queueWaiting) {
			avg += Float.toString(x) + ", ";
		}
		avg += " (total: " + Float.toString(avgWaiting) + ")";
		JTextField astept = new JTextField(avg);
		
		JTextField empty = new JTextField("Secunde/sloturi libere la coada: " + Integer.toString(emptyTime));
		JTextField peak = new JTextField("Secunda de varf: "+ Integer.toString(peakTime));
		
		serv.setAlignmentX(CENTER_ALIGNMENT);
		astept.setAlignmentX(CENTER_ALIGNMENT);
		empty.setAlignmentX(CENTER_ALIGNMENT);
		peak.setAlignmentX(CENTER_ALIGNMENT);
		
		panou.add(serv);
		panou.add(astept);
		panou.add(empty);
		panou.add(peak);
		
		this.pack();
	}
}

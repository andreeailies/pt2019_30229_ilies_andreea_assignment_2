package interfataGrafica;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import pachetControlSimulare.SimulationManager;

public class InputView extends JFrame {


	private static final long serialVersionUID = 1L;
	
	private JPanel panouBaza = new JPanel();
	private JPanel panouAntet = new JPanel();
	private JPanel panouDistanta = new JPanel();
	private JPanel panouServire = new JPanel();
	private JPanel panouNrCozi = new JPanel();
	private JPanel panouNrClienti = new JPanel();
	private JPanel panouTimp = new JPanel();
	
	private JLabel titlu = new JLabel("INTRODUCETI DATELE:");
	
	private JLabel distanta = new JLabel("Distantele dintre clienti:");
	private JLabel minDistLabel = new JLabel("Min:");
	private JLabel maxDistLabel = new JLabel("Max:");
	private JTextField minDist = new JTextField(7);
	private JTextField maxDist = new JTextField(7);
	
	private JLabel servire = new JLabel("Limitele timpilor de servire:");
	private JLabel minServireLabel = new JLabel("Min:");
	private JLabel maxServireLabel = new JLabel("Max:");
	private JTextField minServ = new JTextField(7);
	private JTextField maxServ = new JTextField(7);
	
	private JLabel nrLabel = new JLabel("Numarul de cozi:", SwingConstants.CENTER);
	private JTextField nrCozi = new JTextField(7);
	
	private JLabel nrClienti = new JLabel("Numarul de clienti:", SwingConstants.CENTER);
	private JTextField clienti = new JTextField(7);
	
	private JLabel timpLabel = new JLabel("Timpul de simulare:", SwingConstants.CENTER);
	private JTextField timp = new JTextField(7);

	private JButton enter = new JButton("OK");
	
	public InputView() {
		this.setContentPane(panouBaza);
       
        this.setVisible(true);
        this.setTitle("Introducere date");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(new Dimension(300, 350));
        panouBaza.setSize(new Dimension(300, 400));
        panouBaza.setLayout(new BoxLayout(panouBaza, BoxLayout.Y_AXIS));
        
        panouBaza.add(panouAntet);
        panouBaza.add(panouDistanta);
        panouBaza.add(panouServire);
        panouBaza.add(panouNrCozi);
        panouBaza.add(panouNrClienti);
        panouBaza.add(panouTimp);
        
        antet(panouAntet);
        minMaxLayout(panouDistanta, distanta, minDistLabel, maxDistLabel, minDist, maxDist);
        minMaxLayout(panouServire, servire, minServireLabel, maxServireLabel, minServ, maxServ);
        oneLineLayout(panouNrCozi, nrLabel, nrCozi);
        oneLineLayout(panouNrClienti, nrClienti, clienti);
        oneLineLayout(panouTimp, timpLabel, timp);
        
        panouAntet.setOpaque(false);
        panouDistanta.setBackground(new Color(238, 249, 192));
        panouServire.setBackground(new Color(238, 249, 192));
        panouNrCozi.setOpaque(false);
        panouTimp.setBackground(new Color(238, 249, 192));
        
        panouBaza.setBackground(new Color(66, 215, 244));
        
        panouBaza.add(new JLabel(" "));
        panouBaza.add(enter);
        panouBaza.add(new JLabel(" "));
        enter.setMaximumSize(new Dimension(100, 35));
        enter.setAlignmentX(CENTER_ALIGNMENT);
        this.pack();
        
        listenerButon();
	}
	
	private void antet(JPanel p) {
		p.setMaximumSize(new Dimension(300, 45));
		p.add(titlu);
		titlu.setMinimumSize(new Dimension(300, 25));
		titlu.setFont(new Font("Arial", Font.BOLD, 20));
		titlu.setForeground(Color.white);
	}
	
	private void minMaxLayout(JPanel p, JLabel l, JLabel mmin, JLabel mmax, JTextField textMin, JTextField textMax) {
		p.setMinimumSize(new Dimension(300, 60));
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p.add(l);
		l.setMinimumSize(new Dimension(300, 15));
		l.setAlignmentX(CENTER_ALIGNMENT);
		l.setFont(new Font("Arial", Font.PLAIN, 15));
		
		mmin.setMinimumSize(new Dimension(300, 12));
		mmax.setMinimumSize(new Dimension(300, 12));
		
		JPanel x1 = new JPanel();
		JPanel x2 = new JPanel();
		x1.setBackground(new Color(210, 255, 183));
		x2.setBackground(new Color(210, 255, 183));
		
		x1.setAlignmentX(CENTER_ALIGNMENT);
		x2.setAlignmentX(CENTER_ALIGNMENT);
		
		p.add(x1);
		p.add(x2);
		
		x1.setMaximumSize(new Dimension(300, 40));
		x1.add(mmin);
		x1.add(textMin);
		x2.setMaximumSize(new Dimension(300, 40));
		x2.add(mmax);
		x2.add(textMax);
	}
	
	private void oneLineLayout(JPanel p, JLabel l, JTextField t) {
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p.setMaximumSize(new Dimension(300, 50));
		
		p.add(l);
		p.add(t);
		
		l.setMaximumSize(new Dimension(300, 20));
		l.setAlignmentX(CENTER_ALIGNMENT);
		l.setFont(new Font("Arial", Font.PLAIN, 15));
		
		t.setAlignmentX(CENTER_ALIGNMENT);
		t.setMaximumSize(new Dimension(100, 25));
		t.setFont(new Font("Arial", Font.PLAIN, 15));
		
	}
	
	private void listenerButon() {
		enter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				checkDetails();
			}
			
		});
	}
	
	private void checkDetails() {
		
		int cozi, nrCl, simulare, minD, maxD, minS, maxS;
		try {
			cozi = Integer.parseInt(nrCozi.getText());
			nrCl = Integer.parseInt(clienti.getText());
			minD = Integer.parseInt(minDist.getText());
			maxD = Integer.parseInt(maxDist.getText());
			minS = Integer.parseInt(minServ.getText());
			maxS = Integer.parseInt(maxServ.getText());
			simulare = Integer.parseInt(timp.getText());
					
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Unele date introduse nu sunt numere intregi pozitive!");
			return;
		}
		
		if (cozi <= 0 || nrCl <= 0 || minD < 0 || maxD < minD || minS < 0 || maxS < minS) {
			JOptionPane.showMessageDialog(null, "Date invalide!");
			return;
		}
		
		SimulationManager managerMare = new SimulationManager(simulare, maxS, minS, maxD, minD, cozi,nrCl);
		Thread t = new Thread(managerMare);
		t.start();
	}
}
